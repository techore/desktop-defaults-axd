! Project: aXd
! Location: /etc/X11/Xresources/colors.res
! Description: system xresources file set default colors.
! Usage: if no ~/.Xresources file exists, use these values.
!
!=== aXd nord color scheme
!--- special
*.foreground:   #d8dee9
*.background:   #1c1f27
*.cursorColor:  #d8dee9
!--- black
*.color0:       #3b4252
*.color8:       #4c566a
!--- red
*.color1:       #bf616a
*.color9:       #f17b88
!--- green
*.color2:       #a3b38c
*.color10:      #d3e6b4
!--- yellow
*.color3:       #ebcb8b
*.color11:      #ffdd97
!--- blue
*.color4:       #81a1c1
*.color12:      #a2ccf3
!--- magenta
*.color5:       #b48ead
*.color13:      #e6b6de
!--- cyan
*.color6:       #88c0d0
*.color14:      #99d8e9
!--- white
*.color7:       #e5e9f0
*.color15:      #eef1f6
