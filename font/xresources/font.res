! Project: aXd
! Location: /etc/X11/Xresources/font.res
! Description: set xresources for fonts
! Usage: if no ~/.Xresources file, use these values
!
!=== font
! Update to alter for your account from system-wide defaults
! Xft.dpi: 1080p = 96, 1440p = 128 or 144, 2160p (4k) = 192
Xft.dpi: 96
Xft.autohint: 0
Xft.lcdfilter: lcddefault
Xft.hintstyle: hintfull
Xft.hinting: 1
Xft.antialias: 1
Xft.rgba: rgb
