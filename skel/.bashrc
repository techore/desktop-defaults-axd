# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

#=== aliases
if [ -d /etc/axd/aliases.d ] ; then
  for i in /etc/axd/aliases.d/* ; do
    if [ -r "$i" ]; then
      . "$i"
    fi
  done
  unset i
fi

#=== options
if [ -d /etc/axd/options.d ] ; then
  for i in /etc/axd/options.d/* ; do
    if [ -r "$i" ]; then
      . "$i"
    fi
  done
  unset i
fi

#=== environment variables
if [ -d /etc/axd/envvars.d ] ; then
  for i in /etc/axd/envvars.d/* ; do
    if [ -r "$i" ]; then
      . "$i"
    fi
  done
  unset i
fi

#=== starship prompt
eval "$(starship init bash)"
