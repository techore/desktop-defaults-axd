#=== environment variables
if [ -d /etc/axd/envvars.d ] ; then
  for i in /etc/axd/envvars.d/* ; do
    if [ -r "$i" ]; then
      . "$i"
    fi
  done
  unset i
fi
