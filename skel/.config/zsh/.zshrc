#=== keybinds
#--- ctrl + n | p
bindkey '^n' history-search-forward
bindkey '^p' history-search-backwards

#=== completion
zstyle :compinstall filename "$HOME/.config/zsh/.zshrc"
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
autoload -Uz compinit
compinit

#=== history
HISTFILE=~/.histfile
HISTSIZE=5000
SAVEHIST=5000
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_dups
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_find_no_dups

#=== fzf
#   Need to create an fzf package as a result of the existing Debian fzf package,
#   2024Jun13, does not support --zsh feature.
#   See url for example of using zsh-tab and fzf
#   https://www.youtube.com/watch?v=ud7YxC33Z3w&t=809s

#=== aliases
if [ -d /etc/axd/aliases.d ] ; then
  for i in /etc/axd/aliases.d/* ; do
    if [ -r "$i" ]; then
      . "$i"
    fi
  done
  unset i
fi

#=== options
if [ -d /etc/axd/options.d ] ; then
  for i in /etc/axd/options.d/* ; do
    if [ -r "$i" ]; then
      . "$i"
    fi
  done
  unset i
fi

#=== Lazy-load antidote
#  Check for zsh_plugin.txt, if exists, generate the static load file only
#  when needed.
zsh_plugins=${ZDOTDIR:-$HOME}/.zsh_plugins
if [ -f "${zsh_plugins}.txt" ]; then
  if [ ! "${zsh_plugins}.zsh" -nt "${zsh_plugins}.txt" ]; then
    (
      . /usr/share/zsh-antidote/antidote.zsh
      antidote bundle <"${zsh_plugins}.txt" >"${zsh_plugins}.zsh"
    )
  fi
  . "${zsh_plugins}.zsh"
fi

#=== starship prompt
eval "$(starship init zsh)"
