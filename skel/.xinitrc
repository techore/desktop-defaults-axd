# Project: aXd
# Location: ~/.xinitrc

#=== System-wide xresource files
#--- nord color theme
COLORSRES="/etc/X11/Xresources/colors.res"
if test -f $COLORSRES ; then
   xrdb -merge $COLORSRES
fi
#--- dusk
DUSKRES="/etc/X11/Xresources/dusk.res"
if test -f $DUSKRES ; then
   xrdb -merge $DUSKRES
fi
#--- font
FONTRES="/etc/X11/Xresources/font.res"
if test -f $FONTRES ; then
   xrdb -merge $FONTRES
fi
#--- urxvt
URXVTRES="/etc/X11/Xresources/urxvt.res"
if test -f $URXVTRES ; then
   xrdb -merge $URXVTRES
fi
#--- user xresource file
if test -f ~/.Xresources ; then
   xrdb -merge ~/.Xresources
fi

#=== Terminal daemon
urxvtd -q -f -o &

#=== Settings
#--- screensaver
#  Set screensaver to fifteen minutes.
#  This value is used by slock via xss-lock.
xset s 900 &
#--- power management
#  Set display power management to trigger after thirty minutes.
#  Note using different values is a legacy feature from CRT displays.
#  LCD displays may or may not ignore these.
xset dpms 1800 1800 1800 &
#--- backlight
# Set default brightness for supported backlights.
#brightnessctl set 60% &
#--- volume
#amixer -M set Master 30% &

#=== Desktop wallpaper
#  aXd default background image.
xwallpaper --zoom /usr/share/backgrounds/axd/nord-goldfish-3200x1800.png &
#  aXd author's favorite :)
#xwallpaper --zoom /usr/share/backgrounds/axd/nord-ocean-waves-3840x2160.png &

#=== Daemons
#--- screen lock
xss-lock slock &
#--- automount (udevil)
devmon &
#--- hotkey daemon
sxhkd &
#--- system monitor
#  Using conky to display a welcome and provide essential keybinds
conky -c /etc/conky/axd-welcome.conf
#
#=== Systray apps (daemons)
#--- connman
QT_QPA_PLATFORMTHEME=qt5ct cmst -m &
#--- volume
volumeicon &
#--- clipboard manager
clipit &
#--- redshift
# Where '-l' is latitude:longitude. You can obtain your location using
# https://gps-coordinates.org/ or install geolock-2.0 package. Depending on
# your display, you may need to change arguments. See man page.
#redshift-gtk -l 1.00001:-1.00001 -m randr &

#=== Window manager
exec dusk
