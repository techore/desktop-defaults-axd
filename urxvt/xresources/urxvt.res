! Project: aXd
! Location: /etc/X11/Xresources/urxvt.res
! Description: system xresources file for use by urxvt. 
! Usage: if no ~/.Xresources file exists, use these values.
!
!=== urxvt settings
!--- font and general xettings
URxvt*.font: xft:SauceCodePro Nerd Font Mono:size=12,xft:Terminess Nerd Font Mono:size=12
URxvt*.boldFont: xft:SauceCodePro Nerd Font Mono:bold:size=12,xft:Terminess Nerd Font Mono:size=12
URxvt*.italicFont: xft:SauceCodePro Nerd Font Mono:italic:size=12,xft:Terminess Nerd Font Mono:size=12
URxvt*.bolditalicFont: xft:SauceCodePro Nerd Font Mono:bold:italic:size=12,xft:Terminess Nerd Font Mono:size=12
URxvt*.letterSpace:           0
URxvt*.lineSpace:             0
URxvt*.geometry:              96x32
URxvt*.buffered:              true
URxvt*.iso14755:              false
URxvt*.iso14755_52:           false
URxvt*.tripleclickwords:      true
URxvt*.cursorBlink:           true
URxvt*.cursorUnderline:       true
!--- colors
urxvt.background:             #1C1F27
!urxvtx.background:            #002000
!URxvt*.foreground:            #c0e0e0
!URxvt*.underlineColor:        #30d0e0
!URxvt*.highlightColor:        #808030
!URxvt*.pointerColor:          #d080d0
!URxvt*.cursorColor:           #e0e0e0
!--- scrolling
URxvt*.scrollBar:             false
!URxvt*.scrollBar_right:       true
!URxvt*.scrollBar_floating:    true
URxvt*.scrollstyle:           plain
URxvt*.scrollTtyOutput:       false
URxvt*.scrollWithBuffer:      false
URxvt*.scrollTtyKeypress:     true
URxvt*.secondaryScroll:       true
URxvt*.saveLines:             4096  
!-- shading and fading
!URxvt*.transparent:           false
!URxvt*.shading:               48
URxvt*.fading:                0
!URxvt*.blurRadius:            0
!--- copy/paste using ctrl+shift
URxvt.keysym.C-S-0x43:        eval:selection_to_clipboard
URxvt.keysym.C-S-0x56:        eval:paste_clipboard
!--- perl scripts
URxvt*.perl-ext:              
URxvt*.perl-ext-common:       default,font-size,matcher,selection-to-clipboard
!URxvt*.perl-lib:              /usr/local/lib/urxvt/perl
!URxvt*.keysym.M-s:            searchable-scrollback:start
!URxvt.keysym.C-minus:         perl:font-size:decrease
URxvt.keysym.C-equal:         perl:font-size:increase
!URxvt.keysym.C-0:             perl:font-size:reset
URxvt.keysym.C-question:      perl:font-size:show
!URxvt.resize-font.step:       2
!--- url matcher
URxvt*.url-launcher:          /usr/bin/xdg-open
URxvt*.matcher.button:        1
!URxvt*.keysym.C-Delete:       matcher:last
!URxvt*.keysym.M-Delete:       matcher:list
!--- tabbing menu colors
!URxvt*.tabbed.tabbar-bg:      0
!URxvt*.tabbed.tabbar-fg:      2
!URxvt*.tabbed.tab-bg:         8
!URxvt*.tabbed.tab-fg:         10
