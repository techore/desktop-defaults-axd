! Project: aXd
! Location: /etc/X11/Xresources/dusk.res
! Description: system xresources file for use by dusk. 
! Usage: if no ~/.Xresources file exists, use these values.
!
!=== dusk
!--- fonts
dusk.font1: SauceCodePro Nerd Font Mono:size=12
dusk.font2: Material Icons Outlined:size=12
!--- color theme
#include "/usr/share/dusk/themes/dark/nord-axd.res"
#include "/usr/share/dusk/themes/template.txt"
