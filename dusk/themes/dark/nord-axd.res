! Project: aXd
! Location: /usr/share/dusk/themes/dark/nord-axd.res
! Dependency: dusk 
! Description: Nord color scheme for the dusk window manager.
! Usage: nord-axd.res theme is called from /etc/X11/Xresources/dusk.res,
!   but may be superceded using ~/.Xresources.
! References:
!   https://www.nordtheme.com
!   https://github.com/bakkeby/wiki/Xresources

!=== nord color scheme
#define COLOR0 #2E3440
#define COLOR1 #3B4252
#define COLOR2 #434C5E
#define COLOR3 #4C566A
#define COLOR4 #D8DEE9
#define COLOR5 #E5E9F0
#define COLOR6 #ECEFF4
#define COLOR7 #8FBCBB
#define COLOR8 #88C0D0
#define COLOR9 #81A1C1
#define COLOR10 #5E81AC
#define COLOR11 #BF616A
#define COLOR12 #D08770
#define COLOR13 #EBCB8B
#define COLOR14 #A3BE8C
#define COLOR15 #B48EAD

!=== general colors
#define BASE_FOREGROUND COLOR4
#define BASE_BACKGROUND COLOR0
#define CURSOR COLOR4
!
#define SELECTED_FOREGROUND COLOR6
#define SELECTED_BACKGROUND COLOR10
!#define TITLESEL COLOR2

!=== dusk foreground colors
!#define NORM_FG_COLOR
!#define TITLE_NORM_FG_COLOR 
!#define TITLE_SEL_FG_COLOR
!#define SCRATCH_NORM_FG_COLOR
!#define SCRATCH_SEL_FG_COLOR
#define HID_NORM_FG_COLOR BASE_FOREGROUND
#define HID_SEL_FG_COLOR BASE_FOREGROUND
#define URGENT_FG_COLOR COLOR0
#define MARKED_FG_COLOR COLOR0
#define WS_NORM_FG_COLOR BASE_FOREGROUND
!#define WS_VIS_FG_COLOR
#define WS_SEL_FG_COLOR SELECTED_FOREGROUND
!#define WS_OCC_FG_COLOR
!#define FLOAT_NORM_FG_COLOR
!#define FLOAT_SEL_FG_COLOR

!=== dusk background colors
!#define NORM_BG_COLOR
!#define TITLE_NORM_BG_COLOR
!#define TITLE_SEL_BG_COLOR
!#define SCRATCH_NORM_BG_COLOR
!#define SCRATCH_SEL_BG_COLOR
#define HID_NORM_BG_COLOR COLOR0
#define HID_SEL_BG_COLOR COLOR1
#define URGENT_BG_COLOR COLOR11
#define MARKED_BG_COLOR COLOR4
#define WS_NORM_BG_COLOR COLOR0
!#define WS_VIS_BG_COLOR
#define WS_SEL_BG_COLOR SELECTED_BACKGROUND
!#define WS_OCC_BG_COLOR
!#define FLOAT_NORM_BG_COLOR
!#define FLOAT_SEL_BG_COLOR

!=== dusk border colors
!#define NORM_BORDER_COLOR
!#define TITLE_NORM_BORDER_COLOR
!#define TITLE_SEL_BORDER_COLOR
!#define SCRATCH_NORM_BORDER_COLOR
#define SCRATCH_SEL_BORDER_COLOR COLOR14
!#define URGENT_BORDER_COLOR
!#define MARKED_BORDER_COLOR
