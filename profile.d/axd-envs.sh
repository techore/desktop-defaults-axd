#!/bin/env bash
# Project: aXd
# Location: /etc/profile.d/axd-envs.sh
# Dependency:
# Description: Set bash environment variables.
# Usage: Copy axd-envs.sh to /etc/profile.d/.

#=== aXd exports
#--- update path
if test $(id -u) -ge 1000 ; then
    export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/games:/usr/games"
    if test -d "$HOME/.local/bin" ; then
        export PATH="$HOME/.local/bin:$PATH"
    fi
fi
#--- set neovim as default editor
if [ -f /usr/bin/nvim ]
then
    export EDITOR="nvim"
    export VISUAL="nvim"
fi
#--- set gdk scaling
export GDK_SCALE="1"
export GDK_DPI_SCALE="1.0"
#--- set qt scaling
export QT_AUTO_SCREEN_SCALE_FACTOR="1"
