#!/bin/env bash
# Project: aXd
# Location: /etc/profile.d/axd-dircolors.sh
# Dependency:
# Description: Setup for /bin/ls and /bin/grep to support color. The alias is in /etc/bash.bashrc. 
# Usage: Copy axd-dircolors.sh to /etc/profile.d/.

if test -f "/etc/dircolors" ; then
        eval $(dircolors -b /etc/dircolors)
fi

if test -f "$HOME/.dircolors" ; then
        eval $(dircolors -b $HOME/.dircolors)
fi
